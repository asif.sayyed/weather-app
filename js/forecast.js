
const API_KEY = '9aa905f9841d8d5b68f345f737d1c428';
// const API_KEY = '6e8abe4a012f87cdc8c0a09680b8ca8f'
const BASE_URL = 'http://api.openweathermap.org/data/2.5/forecast'
const units = {
    fahren: 'imperial',
    celsius: 'metric',
    default: 'standard'
};

const getForecast = async(cityName) => {
    const URL = BASE_URL + `?q=${cityName}&appid=${API_KEY}&units=${units.celsius}`;
    const res = await fetch(URL);
    if(res.ok){
        const data = await res.json();
        return data;
    }
    throw new Error(res.status);
}

getForecast('Mumbai').then((data)=> {
    console.log(data);
});